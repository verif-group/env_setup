BSC Setup
==========
```sh
$ sudo apt-get install --no-install-recommends -y ghc libghc-regex-compat-dev libghc-syb-dev \ 
libghc-old-time-dev libghc-split-dev tcl-dev autoconf gperf flex bison
$ git clone https://github.com/B-Lang-org/bsc.git
$ cd bsc
$ git submodule update --init --recursive
$ mkdir -p build                                                                                                              
$ make -j4 install-src PREFIX=$PWD/build
$ make install

Add path in ~/.bashrc
$ export PATH=<install-path>/bsc/inst/bin:$PATH

All the Verilog files will be in build/lib/Verilog

```


 
