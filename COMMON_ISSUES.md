  # Common issues:

  ## gcc error:
  ```sh
  $ sudo add-apt-repository ppa:ubuntu-toolchain-r/test
  $ sudo apt-get update
  $ sudo apt install gcc-10 g++-10
  $ sudo apt install gcc-8 g++-8
  $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 60 --slave /usr/bin/g++ g++ /usr/bin/g++-8
  $ sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-10 40 --slave /usr/bin/g++ g++ /usr/bin/g++-10
  $ sudo update-alternatives --config gcc
  ```

  ## {fabrics, common_bsv} Module not found in devices repo error:
  ```sh
  $ cd <repo-path>/devices
  $ vi .gitmodules 
  $ see the path for fabrics and common_bsv and clone this path devices
  ```

## CI/CD gitlab-runner user credentials error: 
 - ssh setup : https://docs.gitlab.com/ee/ssh/#common-steps-for-generating-an-ssh-key-pair
 - ssh-keygen -t ed25519 -C "<!comment>"
##### The -C flag, with a quoted comment such as an email address, is an optional way to label your SSH keys.

 - You’ll see a response similar to

 ##### Generating public/private ed25519 key pair.
 ```sh
  Enter file in which to save the key (/home/user/.ssh/id_ed25519):
 ```

  ##### Whether you’re creating a ED25519 or an RSA key, you’ve started with the ssh-keygen command. At this point, you’ll see the following message in the command line (for ED25519 keys):

 ##### Generating public/private ed25519 key pair.
 ```sh
  Enter file in which to save the key (/home/user/.ssh/id_ed25519):
 ```

  ##### If you don’t already have an SSH key pair and are not generating a deploy key, accept the suggested file and directory. Your SSH client uses the resulting SSH key pair with no additional configuration.
 
   ##### Alternatively, you can save the new SSH key pair in a different location. You can assign the directory and filename of your choice. You can also dedicate that SSH key pair to a specific host.

 ##### After assigning a file to save your SSH key, you can set up a passphrase for your SSH key:
```sh
 Enter passphrase (empty for no passphrase):
 Enter same passphrase again:
 ```

 ##### If successful, you’ll see confirmation of where the ssh-keygen command saved your identification and private key.


 ##### Adding an SSH key to your GitLab account

 ##### Now you can copy the SSH key you created to your GitLab account. To do so, follow these steps:

    Copy your public SSH key to a location that saves information in text format. 
    The following options saves information for ED25519 keys to the clipboard for the noted operating system:


```sh
$ cat ~/.ssh/id_ed25519.pub | clip
```

######  Select your avatar in the upper right corner, and click Settings
###### Click SSH Keys.
###### Paste the public key that you copied into the Key text box.
###### Make sure your key includes a descriptive name in the Title text box, such as Work Laptop or Home Workstation.
###### Include an (optional) expiry date for the key under “Expires at” section. (Introduced in GitLab 12.9.)
###### Click the Add key button. 



###### Gitlab CI Runner user Issue:
   https://stackoverflow.com/questions/37187899/change-gitlab-ci-runner-user/40703269

