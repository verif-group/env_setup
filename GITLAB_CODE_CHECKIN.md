```sh
# Create Your Private Repository:

# 1. Create a new group
 https://docs.gitlab.com/ee/user/group/#create-a-new-group

# 2. Add users to a group
 https://docs.gitlab.com/ee/user/group/#add-users-to-a-group

# 3. Add projects to a group
 https://docs.gitlab.com/ee/user/group/#add-projects-to-a-group


# Steps for gitlab code-checkin for existing repository:
1. ==> go to the gitlab  project repository       //Skip step for New and Private Repository.
2. new issue -> branch name (title) ->submit      //Skip step for New and Private Repository.
3, ->  create merge request                       //Skip step for New and Private Repository.
4. open terminal : git clone repo-link
5. cd poject-directory
6. git checkout (your-branch-name) 
     or  i.  git fetch origin
         ii. git checkout -b "<your-branch-name>" "origin/<your-branch-name>"  //replace your branch name
7. create your project directory here
8. git add (directory-name)
9. git commit -m " commit -message" directory-name
10. git push origin branch-name

#For Code update:
#Don't create new issue:
for every new file or directory:
8. git add (directory-name or file name)
example: git add file1
for multiple files: git add file1 file2
9. git commit -m " commit -message" directory-name or file name
 example: git commit -m " updated file1 "  file1
 for multiple files: git commit -m " updated file1 file2"  file1 file2
10. git push origin branch-name
 

#update existing files:
9. git commit -m " commit -message" directory-name
10. git push origin branch-name

# Examples:
see all available issues in  verification_environmen repo
https://gitlab.com/shaktiproject/verification_environment/iclass-verif/-/issues
for each issue  there is separate directory:
branch name will issue-number starting with # and issue-name.
for example: https://gitlab.com/shaktiproject/verification_environment/iclass-verif/-/issues/16
branch name is : 16-iclass-soc-simulation-dev
see all codes of branch 16-iclass-soc-simulation-dev  here:
https://gitlab.com/shaktiproject/verification_environment/iclass-verif/-/tree/16-iclass-soc-simulation-dev
```
