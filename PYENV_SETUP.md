# Python dependency setup:
```sh
$ sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
    libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev \
    xz-utils tk-dev libffi-dev liblzma-dev python-openssl git

$  curl -L https://raw.githubusercontent.com/yyuu/pyenv-installer/master/bin/pyenv-installer | bash
```

Add the following at the end of ~/.bashrc
```sh
 export PATH="/home/<username>/.pyenv/bin:$PATH"
 eval "$(pyenv init -)"
 eval "$(pyenv virtualenv-init -)"
 ```

for python 3.6 
```sh 
$ CONFIGURE_OPTS=--enable-shared pyenv install 3.6.10
$ pyenv virtualenv 3.6.10 py36
```

for python 3.7  
```sh
$ CONFIGURE_OPTS=--enable-shared pyenv install 3.7.0
$ pyenv virtualenv 3.7.0 py37
```

Activate env
```sh
$ pyenv activate py36 or py37 

IF ERROR: in bashrc: WARNING: `pyenv init -` no longer sets PATH.
Run `pyenv init` to see the necessary changes to make to your configuration.
SOLUTION:
replace below lines in .bashrc

export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init --path)"
fi

```


Please use the following setup for all CoCoTb based Verification.
- CoCoTb version in pyenv
```sh
$ pip install cocotb==1.4.0
```

