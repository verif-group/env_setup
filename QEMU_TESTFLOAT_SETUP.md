# TESTFLOAT
```sh
$ export RISCV=<install-path>/riscv-gnu-toolchain/build

$ git clone --recursive https://gitlab.com/shaktiproject/verification_environment/common-verif.git
$ cd common-verif

$ cd riscv_test_soft_float/SoftFloat-3e/build/riscv
$ make
$ cd ../../../TestFloat-3e/build/riscv
$ make


testtefloat binary will be created.. add this path to PATH in .bashrc

$ export PATH=<path>:$PATH
add this path to the PATH in .bashrc {export PATH="<install-path>/common-verif/riscv_test_soft_float/TestFloat-3e/build/riscv:$PATH"}
```
# NEW SPIKE
```sh
## NEW_SPIKE_SETUP:
$ git clone https://github.com/riscv/riscv-isa-sim.git
$ cd riscv-isa-sim
$ mkdir build; export RISCV=$PWD/build; cd build
$ ../configure --prefix=$RISCV --enable-commitlog
$ make


## Source Environment path for spike:

$ export PATH="<install-path>/riscv-isa-sim/build:$PATH"

```
# QEMU
```sh
$ sudo apt-get install git libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev
$ sudo apt-get install git-email
$ sudo apt-get install libaio-dev libbluetooth-dev libbrlapi-dev libbz2-dev
$ sudo apt-get install libcap-dev libcap-ng-dev libcurl4-gnutls-dev libgtk-3-dev
$ sudo apt-get install libibverbs-dev libjpeg8-dev libncurses5-dev libnuma-dev
$ sudo apt-get install librbd-dev librdmacm-dev
$ sudo apt-get install libsasl2-dev libsdl1.2-dev libseccomp-dev libsnappy-dev libssh2-1-dev
$ sudo apt-get install libvde-dev libvdeplug-dev libvte-2.90-dev libxen-dev liblzo2-dev
$ sudo apt-get install valgrind xfslibs-dev 
$ sudo apt-get install libnfs-dev libiscsi-dev
$ git clone git://git.qemu-project.org/qemu.git
$ pip install ninja
$ cd qemu
$ mkdir build
$ cd build
$ ../configure --target-list=riscv64-linux-user --enable-debug
$ make -j9

the binary is in riscv64-linux-user/qemu-riscv64  .. 
add this path to the PATH in .bashrc {export PATH="<install-path>/qemu/build/riscv64-linux-user:$PATH"
}

```
