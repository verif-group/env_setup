```sh
#  https://github.com/riscv-collab/riscv-gnu-toolchain
$ sudo apt-get install autoconf automake autotools-dev curl python3 libmpc-dev libmpfr-dev libgmp-dev gawk build-essential bison flex texinfo gperf libtool patchutils bc zlib1g-dev libexpat-dev
$ sudo apt-get install makeinfo textinfo texi2html
$ git clone --recursive https://github.com/riscv/riscv-gnu-toolchain.git 
$ cd riscv-gnu-toolchain
# Installation (Newlib)

#To build the Newlib cross-compiler, pick an install path. If you choose, say, /opt/riscv, then add /opt/riscv/bin to your PATH now. Then, simply run the following command:

$ ./configure --prefix=/opt/riscv
make



#Installation (Linux)
#To build the Linux cross-compiler, pick an install path. If you choose, say, /opt/riscv, then add /opt/riscv/bin to your PATH now. Then, simply run the following command:

$ ./configure --prefix=/opt/riscv
$ make linux







#$ #git checkout 9ef0948
 
#$ #mkdir -p build
#$ #export RISCV=$PWD/build
#$ #cd build
#$ #../configure --prefix=$RISCV
#$ #make 

#Once done.. 
#$ export PATH=<build path>/bin:$PATH
#$ export LD_LIBRARY_PATH=<build_path>/lib/:$LD_LIBRARY_PATH
```
