```sh
# OLD_SPIKE_SETUP:
## common-verif::
$ git clone --recursive https://gitlab.com/shaktiproject/verification_environment/common-verif.git
$ cd common-verif
$ cd spike_model
$ git clone https://github.com/riscv/riscv-isa-sim.git
$ cd riscv-isa-sim
$ git checkout b93262a
$ mkdir build; export RISCV=$PWD/build; cd build
$ ../configure --prefix=$RISCV
$ make
$ cd ../
$ git apply ../spike.patch
$ cd build
$ make
$ cd ../../
$ cmake .
$ make
$ python test_spike_wrapper.py rv64imafdc $PWD/ld/ld.elf
$ source /tools/setup.sh
$ cd ../riscv_test_soft_float/SoftFloat-3e/build/riscv
$ make
$ cd ../../../TestFloat-3e/build/riscv
$ make

$ export RISCV=<install-path>/riscv-gnu-toolchain/build
$ git clone https://gitlab.com/shaktiproject/tools/mod-spike.git
$ sudo apt-get install device-tree-compiler
$ mkdir build
$ cd build
$ ../configure --prefix=$RISCV --with-fesvr=$RISCV --enable-commitlog
$ make
$ [sudo] make install

$ export PATH=<replace-install-path>/mod-spike/build/:$PATH

## NEW_SPIKE_SETUP:
$ git clone https://github.com/riscv/riscv-isa-sim.git
$ cd riscv-isa-sim
$ mkdir build; export RISCV=$PWD/build; cd build
$ ../configure --prefix=$RISCV --enable-commitlog
$ make


## Source Environment path for spike:

$ export PATH="<install-path>/riscv-isa-sim/build:$PATH"


# command to generate spike.dump
$ spike --log-commits --log  spike.dump --isa=rv64imafdc +signature=spike_sig elf-file

```


