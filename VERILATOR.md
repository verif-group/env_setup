```sh
# Verilator Installation

$ sudo apt-get install verilator
# Prerequisites:
$ sudo apt-get install git make autoconf g++ flex bison
$ sudo apt-get install libfl2  # Ubuntu only (ignore if gives error)
$ sudo apt-get install libfl-dev  # Ubuntu only (ignore if gives error)

$ git clone https://github.com/verilator/verilator   # Only first time

$ unset VERILATOR_ROOT  # For bash
$ cd verilator
$ git pull        # Make sure git repository is up-to-date
$ git tag         # See what versions exist
$ git checkout v4.106     # Use development branch (e.g. recent bug fixes) for Cocotb tests 

$ autoconf        # Create ./configure script
$ ./configure
$ make
$ sudo make install
# Now see "man verilator" or online verilator.pdf's for the example tutorial



# To build or run Verilator you need these standard packages:

$ sudo apt-get install perl python3 make
$ sudo apt-get install g++  # Alternatively, clang
$ sudo apt-get install libgz  # Non-Ubuntu (ignore if gives error)
$ sudo apt-get install libfl2 libfl-dev zlibc zlib1g zlib1g-dev  # Ubuntu only (ignore if gives error)

## To build or run the following are optional but should be installed for good performance:

$ sudo apt-get install ccache  # If present at build, needed for run
$ sudo apt-get install libgoogle-perftools-dev numactl perl-doc

```


