# Tools required to install: 
##### MOD-SPIKE [refer: https://gitlab.com/verif-group/env_setup/-/blob/master/SPIKE_SETUP.txt]
```sh
$ git clone https://gitlab.com/shaktiproject/tools/mod-spike.git
$ sudo apt-get install device-tree-compiler
$ mkdir build
$ cd build
$ ../configure --prefix=$RISCV --with-fesvr=$RISCV --enable-commitlog
$ make
$ [sudo] make install
```
##### RISCV-GNU-TOOLCHAIN [refer: https://gitlab.com/verif-group/env_setup/-/blob/master/RISCV_GNU_TOOLS.txt]




##### Build riscv-test: [refer: https://github.com/riscv/riscv-tests]

  We assume that the RISCV environment variable is set to the RISC-V tools
  install path, and that the riscv-gnu-toolchain package is installed.
  ```sh
    $ export RISCV=<install-path>/riscv-gnu-toolchain/build
    $ git clone https://github.com/riscv/riscv-tests
    $ cd riscv-tests
    $ git submodule update --init --recursive
    $ autoconf
    $ ./configure --prefix=$RISCV/target
    $ make
    $ make install
  ```

##### Run riscv-test on spike:
```sh
$ export PATH=<build path>/bin:$PATH
$ export LD_LIBRARY_PATH=<build_path>/lib/:$LD_LIBRARY_PATH
$ export RISCV=<install-path>/riscv-gnu-toolchain/build
$ export PATH=<replace-install-path>/mod-spike/build/:$PATH
$ spike -l --isa=rv64imafdc executable-test-name[elf] 2> spike.log //will generate spike.log 
$ spike --log-commits --log  spike.dump --isa=rv64imafdc +signature=spike_sig elf-file //for new spike.dump
$ spike  -c --isa=rv64imafdc executable-test-name[elf]  // will generate spike.dump  //for old spike.dump
```

